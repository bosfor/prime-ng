import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccordionModule} from 'primeng/accordion';
import {PanelModule} from 'primeng/panel';
import {MenuModule} from 'primeng/menu';
import {MenubarModule} from "primeng/menubar";
import {ToastModule} from "primeng/toast";
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {ContextMenuModule} from "primeng/contextmenu";
import {MultiSelectModule} from "primeng/multiselect";
import {DialogModule} from "primeng/dialog";
import {SliderModule} from "primeng/slider";
import {CalendarModule} from "primeng/calendar";
import {TableModule} from "primeng/table";
import {ProgressBarModule} from "primeng/progressbar";
import {ToggleButtonModule} from "primeng/togglebutton";
import {DropdownModule} from "primeng/dropdown";
import {TreeModule} from "primeng/tree";
import {MessageModule} from "primeng/message";
import {MessagesModule} from "primeng/messages";
import {SidebarModule} from "primeng/sidebar";
import {CardModule} from 'primeng/card';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccordionModule,
    PanelModule,
    MenuModule,
    MenubarModule,
    DropdownModule,
    TableModule,
    CalendarModule,
    SliderModule,
    DialogModule,
    MultiSelectModule,
    ContextMenuModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    ProgressBarModule,
    ToggleButtonModule,
    TreeModule,
    MessageModule,
    MessagesModule,
    SidebarModule,
    CardModule,
  ],
  exports: [
    AccordionModule,
    PanelModule,
    MenuModule,
    TableModule,
    MenubarModule,
    DropdownModule,
    CalendarModule,
    SliderModule,
    DialogModule,
    MultiSelectModule,
    ContextMenuModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    ProgressBarModule,
    ToggleButtonModule,
    TreeModule,MessageModule,
    MessagesModule,
    MessageModule,
    SidebarModule,
    CardModule,

  ]
})
export class PrimengModule { }
