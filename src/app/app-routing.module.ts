import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProfileComponent} from './components/profile/profile.component';
import {TableComponent} from "./components/table/table.component";
import {MessagesComponent} from "./components/messages/messages.component";

const routes: Routes = [
  { path: 'messages', component: MessagesComponent },
  { path: 'table', component: TableComponent },
  { path: 'profile', component: ProfileComponent },
  { path: '', redirectTo: '/table', pathMatch: 'full' },
  { path: '**', redirectTo: '/profile', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
